#!/usr/bin/env python3

import os
import csv
import sys
import yaml
import argparse

from telethon import TelegramClient, events, sync, types

parser = argparse.ArgumentParser(description="""
    Behave as a telegram client. Accepts incoming text and media, and record
    them to a given directory specified.""")

parser.add_argument("config", help="""Config file in yaml.""")

args = parser.parse_args()



config = yaml.load(open(args.config, "r").read())

basepath = os.path.realpath(os.path.dirname(args.config))

FILEPATH_SESSION = os.path.join(basepath, config["session_file"])
FILEPATH_MESSAGES = os.path.join(basepath, config["message_to"])
DIRPATH_DOWNLOAD = os.path.join(basepath, config["download_to"])

SILENCE = True 
if "silent" in config and config["silent"] == False:
    SILENCE = False 

if not os.path.isdir(DIRPATH_DOWNLOAD):
    if not os.path.exists(DIRPATH_DOWNLOAD):
        os.system("mkdir -p %s" % DIRPATH_DOWNLOAD)
    else:
        print("Download path %s exists, however it's not a directory"
            % DIRPATH_DOWNLOAD)
        sys.exit(1)


#-----------------------------------------------------------------------------


async def handleNewMessage(msg, writer):
    if msg.out: return

    row = [
        msg.date.isoformat(),
        msg.from_id,
    ]

    if msg.message:
        #print(msg)
        writer.writerow(row + [
            "message",
            msg.message
        ])
        return True

    if msg.media:
        if isinstance(msg.media, types.MessageMediaGeo) or isinstance(msg.media, types.MessageMediaGeoLive):
            writer.writerow(row + [
                "location",
                "%s,%s" % (msg.media.geo.lat, msg.media.geo.long)
            ])
            return False 

        savepath = await msg.download_media(file=DIRPATH_DOWNLOAD)
        writer.writerow(row + ["media", savepath])
        return True




#-----------------------------------------------------------------------------

with TelegramClient(
    api_id=config["api"]["id"],
    api_hash=config["api"]["hash"],
    session=config["session_file"]
) as client, open(
    FILEPATH_MESSAGES,
    "a",
    buffering=1 # line buffered
) as msgFile:
   
    msgWriter = csv.writer(msgFile)


    me = client.get_me()
    print(me)


    @client.on(events.NewMessage())
    async def handler(event):

        print(event)
        if event.message and event.message.from_id == me.id:
            print("Ignore message from myself.")
            return

        answer = await handleNewMessage(msg=event.message, writer=msgWriter)
        #print(event)

        if not SILENCE:
            await event.message.mark_read()

        if answer and not SILENCE:
            await event.reply('OK.')
    

    client.run_until_disconnected()
